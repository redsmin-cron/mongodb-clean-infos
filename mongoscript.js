OneHourAgo = (+new Date())-(3600*1000);
print('Removing from '+new Date(OneHourAgo));

documentRemovedCount = db.infos.remove({'ts':{'\$lt':OneHourAgo/1000}}).nRemoved;
print('Removed', documentRemovedCount , 'infos');
assert(documentRemovedCount  >= 0);
